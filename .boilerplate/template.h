{%- extends 'base.cpp' -%}
{%- macro fn() -%}{{stem}}.h{%- endmacro -%}
{%- block brief %}Header{% endblock -%}
{%- set include_guard_raw %}INCLUDED_{{fn()}}_GUID_{{ make_guid() }}{% endset %}
{%- macro include_guard() %}{{ ('INCLUDED_' + fn() + '_GUID_' + make_guid()) | make_identifier() }}{% endmacro %}
{% block content %}
#pragma once
#ifndef {{include_guard()}}
#define {{include_guard()}}

// Internal Includes
#include "{{stem}}.h"

// Library Includes
// - none

// Standard Includes
// - none

namespace xrt {

} // namespace xrt

#endif // {{include_guard()}}
{% endblock %}
