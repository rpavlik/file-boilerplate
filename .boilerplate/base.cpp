{% from 'base' import notice -%}
{% block fileheader -%}
/** @file
 *  @brief  {% block brief %}{% endblock %}
 *  @author {{author}}
 */
{% endblock -%}
{{ notice | c_block_comment('/*', ' * ', ' */') }}
{% block content %}{% endblock %}