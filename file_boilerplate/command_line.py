#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018-2019 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Author(s):    Ryan Pavlik <ryan.pavlik@collabora.com>
#
# Purpose:      Generates code files (by default .cpp and .h) with the given stem,
#               containing the expected default/boilerplate contents

import argparse
from file_boilerplate import BoilerplateRun
from sys import exit


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('stem', metavar='stem', nargs='?',
                        help='Specify generated filename stem (file extensions.cpp and .h will be added)')
    parser.add_argument('--noh', action='store_true', default=False,
                        help='Skip generating the .h file')
    parser.add_argument('--nocpp', action='store_true', default=False,
                        help='Skip generating the .cpp file')
    parser.add_argument('-t', '--type', action='append', default=[],
                        help='Specify file types (nominally, extensions) to explicitly use.')
    parser.add_argument('-f', '--force', action='store_true', default=False,
                        help='Write file(s) even if already existing')
    args = parser.parse_args()

    success = True
    run = BoilerplateRun()
    file_types = args.type
    if not file_types:
        if not args.noh:
            file_types.append('h')
        if not args.nocpp:
            file_types.append('cpp')

    for file_type in file_types:
        success = run.render(stem=args.stem,
                             ext=file_type).write(force=args.force) and success

    if success:
        exit()
    exit(-1)
