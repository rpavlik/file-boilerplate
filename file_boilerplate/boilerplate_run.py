#!/usr/bin/python3 -i
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Author(s):    Ryan Pavlik <ryan.pavlik@collabora.com>
#
# Purpose:      Generates a .cpp and .h file with the given stem,
#               containing the expected default/boilerplate contents

from .filters import do_prefix_block, do_make_identifier, do_c_block_comment, do_cpp_line_comment
from .find_paths import find_paths

from jinja2 import Environment, FileSystemLoader
from jinja2.utils import Markup
from datetime import date
from os.path import exists
import subprocess  # to get author from Git
from re import sub  # for guid include guard
from uuid import uuid4  # for guid include guard


class BoilerplateOutput(object):
    def __init__(self, fn, data):
        self.fn = fn
        self.data = data

    def __str__(self):
        return '\n'.join(['Filename: ' + self.fn, 'Contents:', self.data])

    def write(self, force=False):
        if exists(self.fn):
            if force:
                print("Overwriting {} ...".format(self.fn))
            else:
                print('Error: {} already exists (and not forcing)! Not overwriting...'.format(
                    self.fn))
                return False
        else:
            print("Generating {} ...".format(self.fn))
        # Write the output
        with open(self.fn, 'w', encoding='utf-8') as f:
            f.write(self.data)
        return True


def make_guid_include_guard(fn):
    guid = str(uuid4()).upper().replace('-', '_')
    raw_include_guard = "INCLUDED_{fn}_GUID_{guid}".format(fn=fn, guid=guid)
    # Try to make a valid C preprocessor identifier:
    # Convert "bad" characters to _
    return sub(r"[-./]", '_', raw_include_guard)


def make_author():
    return '{name} <{email}>'.format(
        name=subprocess.getoutput('git config user.name'),
        email=subprocess.getoutput('git config user.email'))


def make_year():
    return date.today().year


def do_make_guid():
    return str(uuid4()).upper()


def make_environment():
    env = Environment(keep_trailing_newline=True,
                      autoescape=False, loader=FileSystemLoader(find_paths()))
    env.globals['year'] = make_year()
    env.globals['author'] = make_author()
    env.filters['prefix_block'] = do_prefix_block
    env.filters['make_identifier'] = do_make_identifier
    env.globals['make_guid'] = do_make_guid
    env.filters['c_block_comment'] = do_c_block_comment
    env.filters['cpp_line_comment'] = do_cpp_line_comment
    return env


class BoilerplateRun(object):
    def __init__(self):
        self.env = make_environment()

    def get_env(self):
        return self.env.overlay()

    def render(self, stem, ext):
        env = self.get_env()
        template = env.get_template('template.%s' %
                                    ext, globals=dict(stem=stem))
        output = template.render(year=make_year(), author=make_author())
        fn = template.module.fn()
        return BoilerplateOutput(fn=fn, data=output)
