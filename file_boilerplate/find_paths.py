#!/usr/bin/python3 -i
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Author(s):    Ryan Pavlik <ryan.pavlik@collabora.com>
#
# Purpose:      Create a list of 'search paths' for templates

from pathlib import Path


def get_parents(dir=None):
    if dir is None:
        dir = Path.cwd()

    parts = list(dir.parts)
    ret = [Path(*(parts[:i+1])) for i in range(len(parts))]
    ret.reverse()
    return ret


def get_potential_boilerplate_dirs(dir=None):
    return [d/'.boilerplate' for d in get_parents(dir)]


def find_paths(dir=None):
    return [str(d) for d in get_potential_boilerplate_dirs(dir) if d.exists() and d.is_dir()]
