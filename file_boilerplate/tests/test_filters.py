# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Author(s):    Ryan Pavlik <ryan.pavlik@collabora.com>

from file_boilerplate.filters import *
from nose.tools import eq_

def test_do_prefix_block_no_newline():
    eq_(do_prefix_block('A', line_prefix='prefix'),('prefixA'))
    eq_(do_prefix_block('A', line_prefix='prefix', block_begin='BEGIN'),('BEGINprefixA'))
    eq_(do_prefix_block('A', line_prefix='prefix', block_end='END'),('prefixAEND'))
    eq_(do_prefix_block('A', line_prefix='prefix', block_begin='BEGIN', block_end='END'),('BEGINprefixAEND'))

def test_do_prefix_block_two_full_lines():
    eq_(do_prefix_block('A\nB', line_prefix='prefix'),
        ('prefixA\nprefixB'))
    eq_(do_prefix_block('A\nB', line_prefix='prefix', block_begin='BEGIN'),
        ('BEGINprefixA\nprefixB'))
    eq_(do_prefix_block('A\nB', line_prefix='prefix', block_end='END'),
        ('prefixA\nprefixBEND'))
    eq_(do_prefix_block('A\nB', line_prefix='prefix', block_begin='BEGIN', block_end='END'),
        ('BEGINprefixA\nprefixBEND'))

    # Specifying blank line prefix here shouldn't affect output
    eq_(do_prefix_block('A\nB', line_prefix='prefix', block_begin='BEGIN', block_end='END', blank_line_prefix='blank'),('BEGINprefixA\nprefixBEND'))

def test_do_prefix_block_one_line_with_lf():
    eq_(do_prefix_block('A\n', line_prefix='prefix'),('prefixA\n'))
    eq_(do_prefix_block('A\n', line_prefix='prefix', block_begin='BEGIN'),('BEGINprefixA\n'))
    eq_(do_prefix_block('A\n', line_prefix='prefix', block_end='END'),('prefixA\nEND'))
    eq_(do_prefix_block('A\n', line_prefix='prefix', block_begin='BEGIN', block_end='END'),('BEGINprefixA\nEND'))

    # Specifying blank line prefix here shouldn't affect output
    eq_(do_prefix_block('A\n', line_prefix='prefix', block_begin='BEGIN', block_end='END', blank_line_prefix='blank'),
        ('BEGINprefixA\nEND'))

def test_do_prefix_block_two_full_lines_with_lf():
    eq_(do_prefix_block('A\nB\n', line_prefix='prefix'),
        ('prefixA\nprefixB\n'))
    eq_(do_prefix_block('A\nB\n', line_prefix='prefix', block_begin='BEGIN'),
        ('BEGINprefixA\nprefixB\n'))
    eq_(do_prefix_block('A\nB\n', line_prefix='prefix', block_end='END'),
        ('prefixA\nprefixB\nEND'))
    eq_(do_prefix_block('A\nB\n', line_prefix='prefix', block_begin='BEGIN', block_end='END'),
        ('BEGINprefixA\nprefixB\nEND'))

    # Specifying blank line prefix here shouldn't affect output
    eq_(do_prefix_block('A\nB\n', line_prefix='prefix', block_begin='BEGIN', block_end='END', blank_line_prefix='blank'),
        ('BEGINprefixA\nprefixB\nEND'))

def test_do_prefix_block_full_blank_full():
    eq_(do_prefix_block('A\n\nB\n', line_prefix='prefix'),
        ('prefixA\nprefix\nprefixB\n'))
    eq_(do_prefix_block('A\n\nB\n', line_prefix='prefix', block_begin='BEGIN'),
        ('BEGINprefixA\nprefix\nprefixB\n'))
    eq_(do_prefix_block('A\n\nB\n', line_prefix='prefix', block_end='END'),
        ('prefixA\nprefix\nprefixB\nEND'))
    eq_(do_prefix_block('A\n\nB\n', line_prefix='prefix', block_begin='BEGIN', block_end='END'),
        ('BEGINprefixA\nprefix\nprefixB\nEND'))

    # Specifying blank line prefix here should change middle line
    eq_(do_prefix_block('A\n\nB\n', line_prefix='prefix', block_begin='BEGIN', block_end='END', blank_line_prefix='blank'),
        ('BEGINprefixA\nblank\nprefixB\nEND'))
block_intext = """Copyright (c) 2018 Collabora, Ltd.

SPDX-License-Identifier: Apache-2.0

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License."""
block_expected = """/*
** Copyright (c) 2018 Collabora, Ltd.
**
** SPDX-License-Identifier: Apache-2.0
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/"""

line_intext = """Copyright (c) 2018 Collabora, Ltd.

SPDX-License-Identifier: Apache-2.0"""
line_expected = """// Copyright (c) 2018 Collabora, Ltd.
//
// SPDX-License-Identifier: Apache-2.0"""

def test_real_text():
    eq_(
        do_prefix_block(block_intext, line_prefix='** ', blank_line_prefix='**', block_begin='/*\n', block_end='\n*/'),
        block_expected)

def test_real_text_c_block_comment():
    eq_(
        do_c_block_comment(block_intext, line_prefix='** ', begin_chars='/*\n', end_chars='\n*/'),
        block_expected)

def test_real_text_line():
    eq_(
        do_prefix_block(line_intext, line_prefix='// ', blank_line_prefix='//', block_begin=None, block_end=None),
        line_expected)

def test_real_text_cpp_line_comment():
    eq_(
        do_cpp_line_comment(line_intext),
        line_expected)

def test_identifier():
    eq_(do_make_identifier('asdf'), 'asdf')
    eq_(do_make_identifier('as-df'), 'as_df')
    eq_(do_make_identifier('as/df'), 'as_df')
    eq_(do_make_identifier('as.df'), 'as_df')