# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Author(s):    Ryan Pavlik <ryan.pavlik@collabora.com>

from file_boilerplate.find_paths import *
from pathlib import PurePosixPath as PPP
from nose.tools import eq_

def test_decomp():
    eq_(len(get_parents(PPP('/a/b/c'))), 4)


def test_order():
    eq_(get_parents(PPP('/a/b/c'))[0], PPP('/a/b/c'))
    eq_(get_parents(PPP('/a/b/c'))[1], PPP('/a/b'))
    eq_(get_parents(PPP('/a/b/c'))[2], PPP('/a'))
    eq_(get_parents(PPP('/a/b/c'))[3], PPP('/'))

def test_get_candidates():
    eq_(get_potential_boilerplate_dirs(PPP('/a/b/c'))[0], PPP('/a/b/c/.boilerplate'))
    eq_(get_potential_boilerplate_dirs(PPP('/a/b/c'))[1], PPP('/a/b/.boilerplate'))
    eq_(get_potential_boilerplate_dirs(PPP('/a/b/c'))[2], PPP('/a/.boilerplate'))
    eq_(get_potential_boilerplate_dirs(PPP('/a/b/c'))[3], PPP('/.boilerplate'))