#!/usr/bin/env python3 -i
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
#
# Author(s):    Ryan Pavlik <ryan.pavlik@collabora.com>
#
# Purpose:      Filters useful in generating source code boilerplate


def do_prefix_block(s, line_prefix, block_begin=None, block_end=None, blank_line_prefix=None):
    """Prefix each line of a block of text with a string, as well as applying a begin and end block string.

    Can specify the prefix to be different on blank lines, e.g. to avoid trailing space.
    Mostly for commenting a block of text as appropriate for the language.
    """

    # Inspired by Jinja2's 'indent' filter
    if blank_line_prefix is None:
        # Nothing passed for blank line prefix
        blank_line_prefix = line_prefix.strip()
    newline = u'\n'
    s += newline  # this quirk is necessary for splitlines method

    def filter_line(l):
        if l:
            return line_prefix + l
        return blank_line_prefix + l

    lines = s.splitlines()

    # If there's a trailing LF, we must remove it so it doesn't look like a blank line
    # We re-add it after filtering the lines.
    trailingLF = False
    if lines and not lines[-1]:
        trailingLF = True
        lines = lines[:-1]
    lines = list(map(filter_line, lines))
    if trailingLF:
        lines.append('')
    if block_begin is not None:
        if lines:
            lines[0] = block_begin + lines[0]
        else:
            lines[0] = block_begin

    if block_end is not None:
        if lines:
            lines[-1] = lines[-1] + block_end
        else:
            lines[0] = block_end
    return newline.join(lines)


def do_make_identifier(s, replacement='_'):
    """Transform characters that aren't valid in identifiers to the given alternate character."""
    from re import sub
    return sub(r"[-/. \n]", replacement, s)


def do_c_block_comment(s, begin_chars='/*', line_prefix=' * ', end_chars=' */'):
    """Makes things like this: (with begin_chars='/*', line_prefix=' * ', end_chars=' */')

    /*
     * foo
     *
     * bar
     */
     """
    return do_prefix_block(s, line_prefix=line_prefix, blank_line_prefix=line_prefix.rstrip(),
                           block_begin=(begin_chars + '\n'), block_end=('\n' + end_chars))


def do_cpp_line_comment(s, line_prefix='// '):
    """Makes things like this: (with line_prefix='// ')

    // foo
    //
    // bar
    """
    return do_prefix_block(s, line_prefix=line_prefix, blank_line_prefix=line_prefix.rstrip(),
                           block_begin=None, block_end=None)
