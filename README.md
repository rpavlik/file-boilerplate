# File Boilerplate Tool

## Usage Introduction
Install with:

```sh
python setup.py install
```

just as with any other setuptools-based system.
You'll need to use Python 3.4 or newer.
The Python 3 `jinja2` package is required, and will be installed by setuptools if you lack it.

A `genfile` script/executable wrapper is installed (wrapping `file_boilerplate.command_line.main`).

To generate `MyFile.cpp` and `MyFile.h` in some project,
just run the following from your desired destination directory:

```sh
genfile MyFile
```

Adding `--nocpp` and `--noh` will skip those files in generation as indicated in help.
Only the command line handling in the main file is specific to `.cpp`/`.h` files:
the bulk of the tool was built to generate whatever file types please you,
that functionality is just not yet accessible.

## Usage in a New Project
The actual boilerplate generated in any given directory is based on jinja2 templates.
Templates are loaded from one or more `.boilerplate` subdirectories of the current directory and/or any parent directories.
Typically you'll have just one of these `.boilerplate` subdirectories
in the root directory of a project's source tree to set a project style.

This repo has usable sample contents in its `.boilerplate` directory,
but it **does not** "fall back" to those in any way.
They're just there for testing and as samples.
Copy them to your project and adapt as desired.

## Template Names
The main script looks for a template file named `template.cpp` or `template.h`
(actually just appends the file extension to `template.`).
For uniformity, it's suggested that you use the "template inheritance" method
to use a single base template for a given programming language,
to make license boilerplate uniform, etc.
You can also create the same C++ namespaces in both files this way, etc.
(The samples do this.)

## Template Search Path
The tool goes through the current directory and all parents back to the root.
Any `.boilerplate` subdirectories found are added to the Jinja2 template search path.
Child directories are added before parent directories,
so you can partially or completely override boilerplate for subdirectories if you like.

Thus, if the following directories exist, they will be searched in this order for templates referenced.
Considering a hypothetical `myproj` project that contains a `subproject1` subproject
that wants a different template for some reason (license, etc):

- `~/src/myproj/subproject/.boilerplate`
- `~/src/myproj/.boilerplate`

"Fallback" templates for indiscriminate, generic file generation could be placed in `~/.boilerplate`.
However, if you did this, then you'd want to take extra care to be sure each project's
boilerplate dir contained all referenced templates,
to make sure any projects are complete if they are checked out on another system/user.

## Template Contents
The templates are generally very simple applications of [jinja2 templating][http://jinja.pocoo.org/docs/2.10/templates/].
There are only a few additions to the stock capabilities:

- You **must** define a "macro" called `fn()` in each main template, to tell the app what the filename for generated files should be.
- See sample boilerplate for the other globals and defined filters right now.

## Background
This started as the 2018 re-write of Ryan Pavlik's "quick way to make mostly-blank source code files",
which gets revisited every time he sets up a new development environment.
If you're keeping not count, this is roughly the third such iteration
(originally bash, IIRC, then PHP web service + WSH or bash stub).
This one, however, is public, local-only, portable, and customizable,
so it's likely to be more enduring.