from setuptools import setup

setup(name='file_boilerplate',
      version='0.1',
      description='Tool for generating boilerplate for source code files (license header, include guard, etc.)',
      url='https://gitlab.collabora.com/rpavlik/file-boilerplate',
      author='Ryan Pavlik',
      author_email='ryan.pavlik@collabora.com',
      license='BSL-1.0',
      packages=['file_boilerplate'],
      entry_points={
          'console_scripts': ['genfile=file_boilerplate.command_line:main'],
      },
      install_requires=[
          'jinja2',
      ],
      zip_safe=True,
      test_suite='nose.collector',
      tests_require=['nose'],
      )
